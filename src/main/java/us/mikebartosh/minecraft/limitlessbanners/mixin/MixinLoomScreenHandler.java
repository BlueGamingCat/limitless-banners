package us.mikebartosh.minecraft.limitlessbanners.mixin;

import net.minecraft.screen.LoomScreenHandler;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.ModifyConstant;

@Mixin(LoomScreenHandler.class)
public class MixinLoomScreenHandler {
    @ModifyConstant(method = "onContentChanged", constant = @Constant(intValue = 6))
    private int modifyMaxBannerCount(int original_limit) {
        return 16;
    }
}
