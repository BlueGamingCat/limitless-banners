This is a simple mod that removes the `Too Expensive` text from an anvil. Do note this only removes the text and will not let you take out the item unless the server you are playing on allows it.

This mod is especially useful on https://alinea.gg/ since they have removed the level cap in an anvil.
